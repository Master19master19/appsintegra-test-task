
### `Form usage `
Example



	import React from "react";
	import Form from "....../Form";

	const fields = [
	  {
	    label: "Name",
	    type: "text",
	    name: "name",
	    className: "form-control",
	    labelClassName: "form-label",
	    required: true,
	    minLength: 5,
	    maxLength: 15,
	    errorMessages: {
	      required: "Name field is required",
	      minLength: "Name field length must be at least {x} symbols",
	    },
	  },
	  {
	    label: "Phone",
	    type: "phone",
	    name: "phone",
	  },
	];

	const App = () => {
	  const handleSubmit = (formResponse) => {
	    // handle My Response with ( values = { field_1: '....' , field_2: '.....' , ....  } )
	  };
	  return (
	    <div className="w-75 mx-auto">
	      <Form
	        title="Add new Contact"
	        fields={fields}
	        handleSubmit={handleSubmit}
	      />
	    </div>
	  );
	};


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
