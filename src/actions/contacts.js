export const getContacts = () => {
  // fetch from remote server
  let jsonContacts = localStorage.getItem('contacts') ?? '[]';
  let contacts = JSON.parse(jsonContacts);
  return contacts;
};

export const addContact = (contactData) => {
  // push to remote server
  let jsonContacts = localStorage.getItem('contacts') ?? '[]';
  let contacts = JSON.parse(jsonContacts);
  contacts.push(contactData);
  localStorage.setItem('contacts', JSON.stringify(contacts));
  return contacts;
};
export const removeContactByIndex = (index) => {
  // push to remote server
  let jsonContacts = localStorage.getItem('contacts') ?? '[]';
  let contacts = JSON.parse(jsonContacts);
  contacts.splice(index, 1);
  localStorage.setItem('contacts', JSON.stringify(contacts));
  return contacts;
};
