import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../screens/Home';
import AddContact from '../screens/AddContact';

const routes = [
  {
    path: '/contact/create',
    component: AddContact,
  },
  {
    path: '/',
    component: Home,
  },
];

export default function RouteConfig() {
  return (
    <Switch>
      {routes.map((route, i) => (
        <RouteRender key={i} {...route} />
      ))}
    </Switch>
  );
}

function RouteRender(route) {
  return (
    <Route
      path={route.path}
      render={(props) => <route.component {...props} />}
    />
  );
}
