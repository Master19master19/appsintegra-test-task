import React, { useState, useEffect, useRef } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// Form usage
// import React from "react";
// import Form from "....../Form";

// const fields = [
//   {
//     label: "Name",
//     type: "text",
//     name: "name",
//     className: "form-control",
//     labelClassName: "form-label",
//     required: true,
//     minLength: 5,
//     maxLength: 15,
//     errorMessages: {
//       required: "Name field is required",
//       minLength: "Name field length must be at least {x} symbols",
//     },
//   },
//   {
//     label: "Phone",
//     type: "phone",
//     name: "phone",
//   },
// ];

// const App = () => {
//   const handleSubmit = (formResponse) => {
//     // handle My Response with ( values = { field_1: '....' , field_2: '.....' , ....  } )
//   };
//   return (
//     <div className="w-75 mx-auto">
//       <Form
//         title="Add new Contact"
//         fields={fields}
//         handleSubmit={handleSubmit}
//       />
//     </div>
//   );
// };

// export default App;

const useCustomForm = ({ initialValues, fields, onSubmit, notifyErrors }) => {
  const [values, setValues] = useState(initialValues || {});
  const [errors, setErrors] = useState({});

  const formRendered = useRef(true);

  useEffect(() => {
    if (!formRendered.current) {
      setValues(initialValues);
      setErrors({});
    }
    formRendered.current = false;
  }, [initialValues]);

  useEffect(() => {
    let errors = {};
    for (let key in fields) {
      if (
        fields[key]['required'] &&
        (undefined === values[fields[key]['name']] ||
          values[fields[key]['name']].length < 1)
      ) {
        if (undefined !== fields[key]['errorMessages']) {
          errors[fields[key]['name']] =
            fields[key]['errorMessages']['required'] ??
            `Field '${fields[key]['name']}' is required`;
        } else {
          errors[
            fields[key]['name']
          ] = `Field '${fields[key]['name']}' is required`;
        }
      }
      if (
        fields[key]['minLength'] &&
        undefined !== values[fields[key]['name']] &&
        values[fields[key]['name']].length < fields[key]['minLength']
      ) {
        if (
          undefined !== fields[key]['errorMessages'] &&
          undefined !== fields[key]['errorMessages']['minLength']
        ) {
          errors[fields[key]['name']] =
            fields[key]['errorMessages']['minLength'].replace(
              '{x}',
              fields[key]['minLength']
            ) ??
            `Field length must be at least ${fields[key]['minLength']} symbols`;
        } else {
          errors[
            fields[key]['name']
          ] = `Field length must be at least ${fields[key]['minLength']} symbols`;
        }
      }
      if (
        fields[key]['maxLength'] &&
        undefined !== values[fields[key]['name']] &&
        values[fields[key]['name']].length > fields[key]['maxLength']
      ) {
        if (
          undefined !== fields[key]['errorMessages'] &&
          undefined !== fields[key]['errorMessages']['maxLength']
        ) {
          errors[fields[key]['name']] =
            fields[key]['errorMessages']['maxLength'].replace(
              '{x}',
              fields[key]['maxLength']
            ) ??
            `Field length must be at most ${fields[key]['maxLength']} symbols`;
        } else {
          errors[
            fields[key]['name']
          ] = `Field length must be at most ${fields[key]['maxLength']} symbols`;
        }
      }
      if (fields[key]['type'] === 'phone') {
        if (
          undefined !== values[fields[key]['name']] &&
          !/^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g.test(
            values[fields[key]['name']]
          )
        ) {
          if (
            undefined !== fields[key]['errorMessages'] &&
            undefined !== fields[key]['errorMessages']['phone']
          ) {
            errors[fields[key]['name']] = fields[key]['errorMessages']['phone'];
          } else {
            errors[fields[key]['name']] = 'Please enter a valid phone number';
          }
        }
      }
      if (fields[key]['type'] === 'date') {
        if (
          undefined !== values[fields[key]['name']] &&
          !/^\d{4}-\d{2}-\d{2}$/.test(values[fields[key]['name']])
        ) {
          if (
            undefined !== fields[key]['errorMessages'] &&
            undefined !== fields[key]['errorMessages']['date']
          ) {
            errors[fields[key]['name']] = fields[key]['errorMessages']['date'];
          } else {
            errors[fields[key]['name']] = 'Please enter a valid date';
          }
        }
      }
    }
    setErrors(errors);
  }, [values]);

  const handleChange = (event) => {
    const { target } = event;
    const { name, value } = target;
    event.persist();
    setValues({ ...values, [name]: value });
  };

  const handleErrors = () => {
    for (let key in errors) {
      notifyErrors(errors[key]);
    }
  };
  const handleSubmit = (event) => {
    if (event) event.preventDefault();
    // setErrors({ ...errors });
    handleErrors();
    if (!Object.keys(errors).length) {
      onSubmit({ values });
    }
  };

  return {
    values,
    errors,
    handleChange,
    handleSubmit,
  };
};

const Form = (props) => {
  const notify = (string = 'Error') => toast(string);
  const { values, errors, handleChange, handleSubmit } = useCustomForm({
    initialValues: props.initialValues,
    fields: props.fields,
    onSubmit: props.handleSubmit,
    notifyErrors: notify,
  });

  return (
    <form onSubmit={handleSubmit}>
      {props.title && <h1>{props.title}</h1>}
      {props.fields.map((el, index) => (
        <div className="form-group" key={index}>
          <label className={el.labelClassName}>{el.label}</label>
          <input
            className={el.className}
            type={el.type}
            name={el.name}
            onChange={handleChange}
          />
        </div>
      ))}
      <ToastContainer />
      <button className="btn btn-primary btn-block" type="submit">
        Submit
      </button>
    </form>
  );
};
export default Form;
