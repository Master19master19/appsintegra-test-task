import React from 'react';

import Form from '../../common/Form';
import { addContact } from '../../../actions/contacts';
import { Link, useHistory } from 'react-router-dom';

const fields = [
  {
    label: 'Name',
    type: 'text',
    name: 'name',
    className: 'form-control',
    labelClassName: 'form-label',
    required: true,
    minLength: 5,
    maxLength: 15,
    errorMessages: {
      required: 'Name field is required',
      minLength: 'Name field length must be at least {x} symbols',
    },
  },
  {
    label: 'Phone',
    type: 'phone',
    name: 'phone',
    className: 'form-control',
    labelClassName: 'form-label',
    required: true,
    minLength: 7,
    maxLength: 15,
    errorMessages: {
      phone: 'Phone number is invalid',
    },
  },
  {
    label: 'Date',
    name: 'date',
    type: 'date',
    className: 'form-control',
    labelClassName: 'form-label',
    required: true,
  },
];
const AddContact = () => {
  const history = useHistory();

  const handleSubmit = (formResponse) => {
    let newContact = formResponse.values;
    history.push('/');
    addContact(newContact);
  };
  return (
    <div className="w-75 mx-auto">
      <Form
        title="Add new Contact"
        fields={fields}
        handleSubmit={handleSubmit}
      />
      <Link to="/">
        <button className="btn btn-primary mt-5">Go back</button>
      </Link>
    </div>
  );
};

export default AddContact;
