import React from 'react';
import './index.css';
import { getContacts, removeContactByIndex } from '../../../actions/contacts';

import { Link } from 'react-router-dom';

function App() {
  const [contactsData, setcontactsData] = React.useState([]);
  React.useEffect(() => {
    let contacts = getContacts();
    setcontactsData(contacts);
  }, []);

  const removeContact = (index) => {
    let contacts = removeContactByIndex(index);
    setcontactsData(contacts);
  };

  return (
    <div>
      <div className="row d-flex justify-content-center">
        {contactsData.map((el, index) => {
          return (
            <div
              className="col-xl-2 col-md-5 col-lg-3 my-4 bg-secondary mx-4 py-2 px-0"
              key={index}
            >
              <div className="">
                <div className="col-12">
                  <h5> Name: {el.name} </h5>
                </div>
                <div className="col-12">
                  <h5> Phone: {el.phone} </h5>
                </div>
                <div className="col-12">
                  <h5> Date: {el.date} </h5>
                </div>
                <div className="col-12">
                  <button
                    className="btn btn-danger btn-block"
                    onClick={() => removeContact(index)}
                  >
                    Remove
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <Link to="/contact/create">
        <button className="btn btn-primary">Add new contact</button>
      </Link>
    </div>
  );
}

export default App;
