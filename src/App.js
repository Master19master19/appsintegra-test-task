import React from 'react';
import './App.css';
import RouteConfig from './components/router';

import { BrowserRouter as Router, Link } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <Link to="/">
            <img
              src="https://icons.iconarchive.com/icons/dtafalonso/android-lollipop/512/Contacts-icon.png"
              className="App-logo"
              alt="logo"
            />
          </Link>
        </header>
        <div>
          <RouteConfig />
        </div>
      </div>
    </Router>
  );
}

export default App;
